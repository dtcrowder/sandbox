#define CATCH_CONFIG_MAIN // Tells Catch to provide a main() - only in one cpp file
#include "catch.hpp"

TEST_CASE("Stack must return a reversed string", "[reverseString]") {
	REQUIRE(StackExample::reverseString("Test String" == "gnirtS tseT"));
}



